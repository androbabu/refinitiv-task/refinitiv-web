import styled from "styled-components"

const Card = styled.div`
  background: #fff;
  border-radius: 2px;
  display: inline-block;
  margin: 1rem;
  position: relative;
  height: 100px;
  width: 180px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 18px;
  margin: 8px;
  box-sizing: border-box;
  box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
`;

const Title = styled.div`
  font-size: 18px;
  font-weight: bold;
  color: #777;
`;

const Status = styled.div`
  font-size: 24px;
  font-weight: bold;
  color: ${p => p.healthy ? "#36a65a" : "#df493a"};
  text-align: right;
`;

function HealthCard(props) {
  return (
    <Card>
      <Title>{props.title}</Title>
      <Status healthy={props.status === "up"}>{props.status}</Status>
    </Card>
  )
}

export default HealthCard
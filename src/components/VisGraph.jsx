import { useEffect, useRef } from "react";
import { DataSet } from "vis-data/peer";
import { Network } from "vis-network/peer";
import "vis-network/styles/vis-network.css";

function VisGraph(props) {

  const ref = useRef(null);

  useEffect(() => {
    if (!ref.current) return;

    // create an array with nodes
    const nodes = new DataSet(props.nodes);

    // create an array with edges
    const edges = new DataSet(props.edges);

    // create a network
    const container = ref.current;
    const data = {
      nodes: nodes,
      edges: edges
    };
    const options = {
      edges: {
        arrows: 'to',
        font: {
          size: 12,
        },
        length: 180,
      }
    };
    new Network(container, data, options);
  }, [props.nodes, props.edges])

  return <div ref={ref} style={props.style}></div>
}

export default VisGraph
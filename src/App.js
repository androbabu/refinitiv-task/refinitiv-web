import DataStats from "./views/DataStats";
import Health from "./views/HealthView";

function App() {
  return (
    <div className="App">
      <Health />
      <DataStats />
    </div>
  );
}

export default App;

import { useEffect, useState } from "react";
import Api from "../helpers/Api"
import VisGraph from "../components/VisGraph";

function DataStats() {

  let [stats, setStats] = useState(null);

  useEffect(() => {
    Api.getStats().then(res => {
      let { nodes, relationships } = res.data;

      nodes = nodes.map(n => ({ id: n.identity, label: n.properties.name }));
      let edges = relationships.map(r => ({ from: r.start, to: r.end, label: r.type }))
      setStats({ nodes, edges })
    })
  }, [])

  if (!stats) return null;

  return (
    <VisGraph nodes={stats.nodes} edges={stats.edges} style={{ height: '500px' }} />
  )
}

export default DataStats
import { useEffect, useState } from "react"
import styled from "styled-components"
import HealthCard from "../components/HealthCard";
import Api from "../helpers/Api"

const Container = styled.div`
  margin: 64px 8px 8px 8px;
  display: flex;
  justify-content: center;
`;

function HealthView() {

  const [health, setHealth] = useState({});

  useEffect(() => {
    Api.getHealth().then(res => setHealth(res.data))
  }, [])

  return (
    <Container>
      {Object.entries(health).map(([k, v]) => <HealthCard key={k} title={k} status={v} />)}
    </Container>
  )
}

export default HealthView
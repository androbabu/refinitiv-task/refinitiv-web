## Getting Started

This applications visualize the overview of graph nodes and it's relationship.

## Local Development

Create `.env` file by copying from the `.env.example` file and update the required values

## Deployment

The project is built in Gitlab CI and will be deployed to Firebase.

Deployed Link: [https://refinitiv-offshore-leaks.web.app](https://refinitiv-offshore-leaks.web.app)
